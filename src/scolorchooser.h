#ifndef SCOLORCHOOSER_H
#define SCOLORCHOOSER_H

#include <QDebug>

#include <QDesktopWidget>
#include <QImage>
#include <QLinearGradient>
#include <QMainWindow>
#include <QMouseEvent>
#include <QPainter>
#include <QRect>
#include <QScopedPointer>
#include <QTimer>


class SColorChooser : public QWidget
{
    Q_OBJECT

public:
    explicit SColorChooser(QWidget *parent = nullptr);
    ~SColorChooser() override;

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    bool idleMouse = false;

    int mouseX;
    int mouseY;
    int screenW;
    int screenH;

    float hue;
    float sat;

    QLinearGradient colorGradient;

    QBrush brush;
    QImage image;
    QImage mouseImage;
    QPen pen;
    QPixmap pixmap;
    QPixmap mousePixmap;
    QTimer timer;
    QTimer mouseTimer;

    void mouseMoveEvent(QMouseEvent *e) override;
    void mousePressEvent(QMouseEvent *e) override;

private slots:    
    void updateColor();
    void checkMouseIdle();
    
signals:
    void colorSelected(const QString& color);
};

#endif // SCOLORCHOOSER_H
