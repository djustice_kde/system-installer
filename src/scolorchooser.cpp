#include "scolorchooser.h"

#include <QApplication>

SColorChooser::SColorChooser(QWidget *parent) :
    QWidget(parent)
{
    QSize screenSize = qApp->desktop()->size();
    screenW = screenSize.width();
    screenH = screenSize.height();

    setMaximumWidth(screenW);
    setMaximumHeight(screenH);
    setWindowFlags(Qt::FramelessWindowHint);
    setWindowState(Qt::WindowFullScreen);

    setMouseTracking(true);
    grabMouse();
    
    image = QImage(":/images/bg.png");
    pixmap = QPixmap::fromImage(image);
    
    mouseImage = QImage(":/images/mouse_move.png");
    mousePixmap = QPixmap::fromImage(mouseImage);
    
    timer.setInterval(200);
    connect(&timer, SIGNAL(timeout()), this, SLOT(updateColor()));
    
    mouseTimer.setInterval(1500);
    connect(&mouseTimer, SIGNAL(timeout()), this, SLOT(checkMouseIdle()));
    
    pen = QPen(Qt::NoPen);
    brush = QBrush(QColor(255,255,255));
}

void SColorChooser::mouseMoveEvent(QMouseEvent *event) {
    idleMouse = false;

    mouseX = event->pos().x();
    mouseY = event->pos().y();
    hue = (float) mouseX / screenW;
    sat = (float) mouseY / screenH;
    
    mouseImage = QImage(":/images/mouse_move.png");
    mousePixmap = QPixmap::fromImage(mouseImage);

    if (!timer.isActive())
        timer.start();

    if (!mouseTimer.isActive())
        mouseTimer.start();

    update();
}

void SColorChooser::mousePressEvent(QMouseEvent *event) {
    qDebug() << brush.color().name();
    emit colorSelected(brush.color().name());
    releaseMouse();
    hide();
    close();
}

void SColorChooser::updateColor() {
    if ((mouseX > 0 && mouseX < 7) && (mouseY > 0 && mouseY < 7)) {
        close();
    } else if (mouseX > screenW || mouseY > screenH) {
        return;
    }

    if (hue > 0.001 && hue < 0.1) {
        brush = QBrush(QColor(Qt::red));
    } else if (hue > 0.1 && hue < 0.15) {
        brush = QBrush(QColor(255,60,0));
    } else if (hue > 0.15 && hue < 0.20) {
        brush = QBrush(QColor(255,120,0));
    } else if (hue > 0.20 && hue < 0.25) {
        brush = QBrush(QColor(255,160,0));
    } else if (hue > 0.25 && hue < 0.30) {
        brush = QBrush(QColor(Qt::yellow));
    } else if (hue > 0.30 && hue < 0.35) {
        brush = QBrush(QColor(160,255,0));
    } else if (hue > 0.35 && hue < 0.40) {
        brush = QBrush(QColor(120,255,0));
    } else if (hue > 0.40 && hue < 0.45) {
        brush = QBrush(QColor(40,255,0));
    } else if (hue > 0.45 && hue < 0.50) {
        brush = QBrush(QColor(Qt::green));
    } else if (hue > 0.50 && hue < 0.55) {
        brush = QBrush(QColor(0,255,120));
    } else if (hue > 0.55 && hue < 0.60) {
        brush = QBrush(QColor(0,180,255));
    } else if (hue > 0.60 && hue < 0.65) {
        brush = QBrush(QColor(0,120,255));
    } else if (hue > 0.65 && hue < 0.70) {
        brush = QBrush(QColor(0,40,255));
    } else if (hue > 0.70 && hue < 0.75) {
        brush = QBrush(QColor(Qt::blue));
    } else if (hue > 0.75 && hue < 0.80) {
        brush = QBrush(QColor(180,0,255));
    } else if (hue > 0.80 && hue < 0.85) {
        brush = QBrush(QColor(255,0,255));
    } else if (hue > 0.85 && hue < 0.90) {
        brush = QBrush(QColor(255,0,180));
    } else if (hue > 0.90 && hue < 1.0) {
        brush = QBrush(QColor(Qt::red));
    }

    if (sat > 0.001 && sat < 0.25) {
        brush.setColor(brush.color().darker());
    } else if (sat > 0.75 && sat < 0.85) {
        brush.setColor(brush.color().lighter());
    } else if (sat > 0.85 && sat < 1.0) {
        brush.setColor(brush.color().lighter().lighter());
    }
    
    update();
}

void SColorChooser::checkMouseIdle() {
    idleMouse = true;
    mouseImage = QImage(":/images/mouse_click");
    mousePixmap = QPixmap::fromImage(mouseImage);
    update();
}

void SColorChooser::paintEvent(QPaintEvent* event) {
    QPainter painter(this);
    painter.setPen(pen);
    painter.setBrush(brush);

    QRect rect(0,0,screenW,screenH);
    painter.drawPixmap(0, 0, screenW, screenH, pixmap);
    if (mouseX > 0 && mouseY > 0)
        painter.drawPixmap(mouseX - 50, mouseY - 29, 100, 58, mousePixmap);
    painter.setCompositionMode(QPainter::CompositionMode_Multiply);
    painter.drawRect(rect);

    QRadialGradient rGrad(mouseX, mouseY, screenW / 3);
    rGrad.setColorAt(0, brush.color());
    rGrad.setColorAt(1, QColor(0,0,0,220));
    painter.setBrush(rGrad);
    painter.drawRect(rect);
}

SColorChooser::~SColorChooser() = default;
