#include "systeminstaller.h"
#include "ui_systeminstaller.h"

SystemInstaller::SystemInstaller(QWidget *parent) :
    QWidget(parent),
    m_ui(new Ui::SystemInstaller)
{
    m_ui->setupUi(this);
    
    setGeometry(
    QStyle::alignedRect(
        Qt::LeftToRight,
        Qt::AlignCenter,
        this->size(),
        QApplication::desktop()->geometry()
    ));

    // TIMEZONE LIST
    QFile timeZoneFile(":/config/timezones");
    if (!timeZoneFile.open(QIODevice::ReadOnly))
        qDebug() << "error reading timezone data";
    
    timeZoneModel = new QStringListModel(this);
    QTextStream tzStream(&timeZoneFile);
    while (!tzStream.atEnd()) {
        QString line = tzStream.readLine();
        timeZoneStringList.append(line.split(":").at(0));
        timeZoneDataList.append(line);
    }

    timeZoneModel->setStringList(timeZoneStringList);
    m_ui->timeZoneView->setModel(timeZoneModel);
    m_ui->timeZoneView->setEnabled(true);

    connect(m_ui->timeZoneView->selectionModel(), 
      SIGNAL(selectionChanged(QItemSelection,QItemSelection)), 
      this, SLOT(timeZoneSelectionChanged(QItemSelection)));

    // LANGUAGE LIST
    QFile languagesFile(":/config/languages");
    if (!languagesFile.open(QIODevice::ReadOnly))
        qDebug() << "error reading languages data";

    languageModel = new QStringListModel(this);
    QTextStream langStream(&languagesFile);
    while (!langStream.atEnd()) {
        QString line = langStream.readLine();
        languageStringList.append(line.split(":").at(1));
        languageDataList.append(line);
    }

    languageModel->setStringList(languageStringList);
    m_ui->languageView->setModel(languageModel);
    m_ui->languageView->setEnabled(true);

    connect(m_ui->languageView->selectionModel(), 
      SIGNAL(selectionChanged(QItemSelection,QItemSelection)), 
      this, SLOT(languageSelectionChanged(QItemSelection)));
    
    // DATE/TIME
    m_ui->dateTimeEdit->setDateTime(QDateTime::currentDateTime());

    // NEXT/PREVIOUS/QUIT BUTTONS
    connect(m_ui->nextButton, SIGNAL(clicked()), this, SLOT(nextButtonClicked()));
    connect(m_ui->previousButton, SIGNAL(clicked()), this, SLOT(previousButtonClicked()));
    connect(m_ui->quitButton, SIGNAL(clicked()), this, SLOT(quitButtonClicked()));
    
    // NETWORK/USER/COLOR/SOFTWARE/DESTINATION BUTTONS
    connect(m_ui->networkButton, SIGNAL(clicked()), this, SLOT(networkButtonClicked()));
    connect(m_ui->userButton, SIGNAL(clicked()), this, SLOT(userButtonClicked()));
    connect(m_ui->colorButton, SIGNAL(clicked()), this, SLOT(colorButtonClicked()));
    connect(m_ui->softwareButton, SIGNAL(clicked()), this, SLOT(softwareButtonClicked()));
    connect(m_ui->destinationButton, SIGNAL(clicked()), this, SLOT(destinationButtonClicked()));

    // NETWORK
    QNetworkAccessManager nam;
    QNetworkRequest req(QUrl("http://www.google.com"));
    QNetworkReply* reply = nam.get(req);
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    if (reply->bytesAvailable()) {
        m_ui->networkStatusCheckLabel->setPixmap(QPixmap::fromImage(QImage(":/images/check.png")));
        m_ui->networkStatusLabel->setText("Network connected");
        m_ui->networkButton->setStyleSheet("font: bold;");
        m_ui->userButton->setEnabled(true);
    } else {
        m_ui->networkStatusCheckLabel->setPixmap(QPixmap::fromImage(QImage(":/images/quit.png")));
        m_ui->networkStatusLabel->setText("Network disconnected");
    }
    networkTimer.setInterval(6000);
    connect(&networkTimer, SIGNAL(timeout()), this, SLOT(checkNetwork()));
    networkTimer.start();

    connect(m_ui->hostnameLine, SIGNAL(textChanged(const QString&)), this, SLOT(hostnameLineChanged(const QString&)));
    
    // USER
    connect(m_ui->displayNameLine, SIGNAL(textChanged(const QString&)), this, SLOT(validateUserPage()));
    connect(m_ui->loginLine, SIGNAL(textChanged(const QString&)), this, SLOT(validateUserPage()));
    connect(m_ui->passwordLine, SIGNAL(textChanged(const QString&)), this, SLOT(validateUserPage()));
    connect(m_ui->confirmPasswordLine, SIGNAL(textChanged(const QString&)), this, SLOT(validateUserPage()));
    
    
    // DESTINATION
    connect(m_ui->gpartedButton, SIGNAL(clicked()), this, SLOT(gpartedButtonClicked()));
    connect(m_ui->kdepartButton, SIGNAL(clicked()), this, SLOT(kdepartButtonClicked()));
}

void SystemInstaller::timeZoneSelectionChanged(const QItemSelection & item)
{
    QModelIndex index = item.indexes().first();
    QString clickedItem = timeZoneDataList.at(index.row());
    QString clickedItemCity = index.data().toString().split(":").at(0).split("/").at(1);
    QString clickedItemKeyMap = clickedItem.split(":").at(2);
    if (clickedItemKeyMap.contains(","))
        clickedItemKeyMap = clickedItemKeyMap.split(",").at(0);
    QString clickedItemLocale = clickedItem.split(":").at(1);
    if (clickedItemLocale.contains(","))
        clickedItemLocale = clickedItemLocale.split(",").at(1);
    m_ui->timeZoneLabel->setText("Time Zone: <b>" + clickedItemCity.replace("_", " "));
    m_ui->keyboardLanguageButton->setText(clickedItemKeyMap);
    int row = 0;
    foreach (QString lang, languageDataList) {
        if (lang.split(":").at(0) == clickedItemKeyMap) {
            QModelIndex langIndex = languageModel->index(row, 0);
            m_ui->languageView->setCurrentIndex(langIndex);
            qDebug() << lang;
            return;
        }
        row++;
    }
}

void SystemInstaller::languageSelectionChanged(const QItemSelection & item)
{
    QModelIndex index = item.indexes().first();
    QString clickedItem = languageDataList.at(index.row());
    m_ui->languageLabel->setText("Language: <b>" + clickedItem.split(":").at(1));
}

void SystemInstaller::quitButtonClicked() {
    close();
}

void SystemInstaller::nextButtonClicked() {
    if (m_ui->stackedWidget->currentWidget() == m_ui->localePage) {
        m_ui->stackedWidget->setCurrentWidget(m_ui->configPage);
        m_ui->networkButton->setFocus();
        m_ui->nextButton->setEnabled(false);
    }
}

void SystemInstaller::previousButtonClicked() {
    m_ui->stackedWidget->setCurrentWidget(m_ui->localePage);
    m_ui->nextButton->setEnabled(true);
}

void SystemInstaller::networkButtonClicked() {
    m_ui->configStackedWidget->setCurrentWidget(m_ui->networkPage);
}

void SystemInstaller::userButtonClicked() {
    m_ui->configStackedWidget->setCurrentWidget(m_ui->userPage);
}

void SystemInstaller::colorButtonClicked() {
    SColorChooser *colorChooser = new SColorChooser(0);
    connect(colorChooser, SIGNAL(colorSelected(const QString&)), this, SLOT(colorSelected(const QString&)));
    colorChooser->show();
}

void SystemInstaller::softwareButtonClicked() {
    m_ui->configStackedWidget->setCurrentWidget(m_ui->softwarePage);
    m_ui->softwareButton->setStyleSheet("font: bold;");
    m_ui->destinationButton->setEnabled(true);
}

void SystemInstaller::destinationButtonClicked() {
    m_ui->configStackedWidget->setCurrentWidget(m_ui->destinationPage);

    m_ui->driveCombo->clear();
    m_ui->rootCombo->clear();
    m_ui->rootCombo->addItem(" ");
    m_ui->swapCombo->clear();
    m_ui->swapCombo->addItem(" ");
    
    QProcess p;
    p.start("lsblk -r");
    p.waitForFinished();

    QString output(p.readAllStandardOutput());
    foreach (QString line, output.split("\n")) {
        if (line.contains("disk")) {
            m_ui->driveCombo->addItem(line.left(3) + " (" + line.split(" ").at(3) + ")");
        }
    }
    m_ui->driveCombo->setCurrentIndex(0);
    foreach (QString line, output.split("\n")) {
        if (line.contains("part")) {
            if (line.left(3) == m_ui->driveCombo->currentText().left(3)) {
                QString part = line.split(" ").at(0);
                QString partSize = line.split(" ").at(3);
                m_ui->rootCombo->addItem("/dev/" + part + " (" + partSize + ")");
                m_ui->swapCombo->addItem("/dev/" + part + " (" + partSize + ")");
            }
        }
    }
    m_ui->rootCombo->setCurrentIndex(0);
    m_ui->swapCombo->setCurrentIndex(0);
}

void SystemInstaller::checkNetwork() {
    QNetworkAccessManager nam;
    QNetworkRequest req(QUrl("http://www.google.com"));
    QNetworkReply* reply = nam.get(req);
    QEventLoop loop;
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    if (reply->bytesAvailable()) {
        m_ui->networkButton->setStyleSheet("font: bold;");
        m_ui->userButton->setEnabled(true);
        m_ui->networkStatusCheckLabel->setPixmap(QPixmap::fromImage(QImage(":/images/check.png")));
        m_ui->networkStatusLabel->setText("Network connected");
    } else {
        m_ui->networkStatusCheckLabel->setPixmap(QPixmap::fromImage(QImage(":/images/quit.png")));
        m_ui->networkStatusLabel->setText("Network disconnected");
    }
}

void SystemInstaller::hostnameLineChanged(const QString& text) {
    if (!text.isEmpty() && text.length() > 1 && !text.contains(" ")) {
        m_ui->networkButton->setStyleSheet("font: bold;");
        m_ui->userButton->setEnabled(true);
        m_ui->userButton->setStyleSheet("");
    }
}

void SystemInstaller::validateUserPage() {
    if (m_ui->displayNameLine->text().isEmpty() ||
        m_ui->loginLine->text().isEmpty() ||
        m_ui->passwordLine->text().isEmpty() ||
        m_ui->confirmPasswordLine->text().isEmpty() || 
        m_ui->loginLine->text().contains(" ") ||
        m_ui->passwordLine->text() != m_ui->confirmPasswordLine->text()) {
        m_ui->colorButton->setEnabled(false);
        return;
    }

    m_ui->userButton->setStyleSheet("font: bold;");
    m_ui->colorButton->setEnabled(true);
}

void SystemInstaller::colorSelected(const QString& color)
{
    m_ui->colorButton->setStyleSheet("color: " + color + "; font: bold;");
    m_ui->softwareButton->setEnabled(true);
    
//    kwriteconfig5 --file ~/.config/kdeglobals --group "Colors:Window" --key "BackgroundNormal" "255,0,0"

//    QDBusMessage message = QDBusMessage::createSignal(QStringLiteral("/KGlobalSettings"), QStringLiteral("org.kde.KGlobalSettings"), QStringLiteral("notifyChange"));
//    message.setArguments({ 0, 0 });
//    QDBusConnection::sessionBus().send(message);
}

void SystemInstaller::cfdiskButtonClicked() {
    QProcess p;
    p.start("konsole -e sudo cfdisk");
    p.waitForFinished();
    destinationButtonClicked();
}

void SystemInstaller::gpartedButtonClicked() {
    QProcess p;
    p.start("gparted");
    p.waitForFinished();
    destinationButtonClicked();
}

void SystemInstaller::kdepartButtonClicked() {
    QProcess p;
    p.start("partitionmanager");
    p.waitForFinished();
    destinationButtonClicked();
}

SystemInstaller::~SystemInstaller() = default;
