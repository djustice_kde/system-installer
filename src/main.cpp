#include "systeminstaller.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    SystemInstaller w;
    w.show();

    return app.exec();
}

