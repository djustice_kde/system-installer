#ifndef SYSTEMINSTALLER_H
#define SYSTEMINSTALLER_H

#include <QDebug>

#include <QtWidgets>
#include <QtNetwork>

#include <QAbstractItemView>
#include <QByteArray>
#include <QDesktopWidget>
#include <QEventLoop>
#include <QFile>
#include <QImage>
#include <QItemSelection>
#include <QLabel>
#include <QMessageBox>
#include <QModelIndex>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QPixmap>
#include <QProcess>
#include <QScopedPointer>
#include <QStringList>
#include <QStringListModel>
#include <QTextStream>
#include <QUrl>
#include <QVBoxLayout>
#include <QWidget>

#include "scolorchooser.h"

namespace Ui {
class SystemInstaller;
}

class SystemInstaller : public QWidget
{
    Q_OBJECT

public:
    explicit SystemInstaller(QWidget *parent = nullptr);
    ~SystemInstaller() override;

    QString locale;
    QString keymap;
    QString hostname;

private:
    QStringListModel* timeZoneModel;
    QStringList timeZoneStringList;
    QStringList timeZoneDataList;

    QStringListModel* languageModel;
    QStringList languageStringList;
    QStringList languageDataList;
    
    QNetworkAccessManager networkAccessManager;
    QNetworkRequest networkRequest;
    QNetworkReply *networkReply;
    
    QTimer networkTimer;
    QScopedPointer<Ui::SystemInstaller> m_ui;

private slots:
    void timeZoneSelectionChanged(const QItemSelection&);
    void languageSelectionChanged(const QItemSelection&);
    
    void nextButtonClicked();
    void previousButtonClicked();
    void quitButtonClicked();
    
    void networkButtonClicked();
    void userButtonClicked();
    void colorButtonClicked();
    void softwareButtonClicked();
    void destinationButtonClicked();

    void checkNetwork();
    void hostnameLineChanged(const QString& text);
    
    void validateUserPage();
    
    void colorSelected(const QString& color);
    
    void cfdiskButtonClicked();
    void gpartedButtonClicked();
    void kdepartButtonClicked();
};

#endif // SYSTEMINSTALLER_H
